"""
astar
~~~~~

A* pathfinding algorithm
========================

Usage:
------

Base your tile objects on the AstarTile class.

The astar function accepts the following parameters:

**start**: the tile object to start from.
**end**: the tile where you want to find a path to.
**neighbor_func**: a callable that finds all neighbors to a tile.
**heuristic_func**: a callable that estimates the cost to the goal.

For square grids you can use the heuristics in Heuristics.

If your tile objects have a cost property, the algorithm considers those to
find the cheapest way to the goal (TODO not optimal!).
"""
import heapq


class Heuristics:
    """Some default heuristic callables for square grids"""

    @staticmethod
    def manhattan(start, end):
        """for horizontal and vertical movement only"""
        dx = abs(start.x - end.x)
        dy = abs(start.y - end.y)
        h = (dx + dy) * 10
        return h

    @staticmethod
    def diagonal(start, end):
        """allow diagonal movement"""
        dx = abs(start.x - end.x)
        dy = abs(start.y - end.y)
        if dx > dy:
            h = 14 * dy + (dx - dy) * 10
        else:
            h = 14 * dx + (dy - dx) * 10
        return h


class AstarTile(object):
    """The algorithm needs these properties.
    You can implement them in your class or use this as a base class.
    I tried managing them in a temporary dictionary but that increased
    the runtime by about 20%.
    """
    def __init__(self):
        self._astar_parent = None
        self._astar_h = 0
        self._astar_g = 0
        self._astar_f = 0
        self.mov_cost = 0


def astar(start, end, neighbor_func, heuristic_func=None):
    """A* pathfinding algorithm."""

    modified_tiles = set()

    def update_neighbor(neighbor, current):
        modified_tiles.add(neighbor)
        mov_cost = neighbor.mov_cost * 10
        neighbor._astar_g = current._astar_g + 10 + mov_cost
        neighbor._astar_h = heuristic_func(neighbor, end) \
            if heuristic_func else 0
        neighbor._astar_f = neighbor._astar_h + neighbor._astar_g
        neighbor._astar_parent = current

    open_list = []
    closed_list = set()
    heapq.heappush(open_list, (start._astar_f, start))
    while open_list:
        current = heapq.heappop(open_list)[1]
        closed_list.add(current)

        if current is end:
            break  # found the path!

        for neighbor in neighbor_func(current):
            if neighbor.reachable and neighbor not in closed_list:
                if (neighbor._astar_f, neighbor) in open_list:
                    if neighbor._astar_g > current._astar_g + 10:
                        update_neighbor(neighbor, current)
                else:
                    update_neighbor(neighbor, current)
                    heapq.heappush(open_list, (neighbor._astar_f, neighbor))

    # run through all parents to retrace the path
    path = []
    current = end
    while current._astar_parent is not None:
        current = current._astar_parent
        path.append(current)

    if len(path) == 0:  # no path found
        return None

    path.append(end)

    # reset attributes for the next run
    for tile in modified_tiles:
        tile._astar_parent = None
        tile._astar_h = 0
        tile._astar_g = 0
        tile._astar_f = 0

    return path[::-1]  # reversed (start -> end)
