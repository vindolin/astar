import random
from sys import argv
from astar import AstarTile, astar, Heuristics
import timeit


class Tile(AstarTile):
    def __init__(self, x, y, reachable, mov_cost):
        AstarTile.__init__(self)
        self.x = x
        self.y = y
        self.reachable = reachable
        self.mov_cost = mov_cost

    def __eq__(self, other):
        return (self.x, self.y) == (other.x, other.y)

    def __hash__(self):
        return hash((self.x, self.y))

    def __lt__(self, other):
        return self.__hash__() < other.__hash__()


class Map(dict):

    def __init__(self, allow_diagonal=False):
        super(Map, self).__init__()
        self.allow_diagonal = allow_diagonal
        self._directions = ((0, -1), (1, 0), (0, 1), (-1, 0))
        if allow_diagonal:
            self._directions += ((1, 1), (-1, 1), (1, -1), (-1, -1))

    def get_neighbors(self, current):
        x, y = current.x, current.y
        neighbors = []
        for direction in self._directions:
            coords = (x + direction[0], y + direction[1])
            if coords in self:
                neighbors.append(self[coords])

        return neighbors

    def get_heuristic(self, start, end):
        if self.allow_diagonal:
            h = Heuristics.diagonal(start, end)
        else:
            h = Heuristics.manhattan(start, end)

        return h


TERRAIN = {
    'M': 0,
    'X': 0,
    'b': 1,
    'g': 1,
    'p': 1,
    'w': 4,
    'o': 3,
}

WALLS = ('M', 'X')


def make_map_str(width, height):
    map_str = ''
    for y in range(height):
        line = ''
        for x in range(width):
            char = random.choice(list(TERRAIN.keys()))
            line += char
        map_str += line + '\n'

    return map_str


def make_map(map_str, allow_diagonal=False):
    lines = [line.strip() for line in map_str.strip().splitlines()]
    map_ = Map(allow_diagonal)

    for y, line in enumerate(lines):
        for x, char in enumerate(line):
            map_[(x, y)] = Tile(
                x,
                y,
                0 if char in WALLS else 1,
                TERRAIN[char]
            )
    return map_


def print_result(map_str, path):
    path = [(n.x, n.y) for n in path]
    map_str = [line.strip() for line in map_str.strip().splitlines()]

    result = '\033[0m'
    for y, line in enumerate(map_str):
        for x, char in enumerate(line):
            if (x, y) in path:
                result += '\033[30;42m%s\033[0m' % char
            elif char in WALLS:
                result += '\033[30m%s\033[0m' % char
            else:
                result += char
        result += '\n'

    print(result)


try:
    seed = int(argv[1])
except IndexError:
    seed = random.randint(0, 10000)

try:
    with open('test_map') as _:
        pass
except IOError:
    print('X')
    seed = 1

random.seed(seed)
path = None
# width, height = 80, 40
width, height = 40, 20

while True:

    if seed == 0:
        seed, start_y, end_y, map_str = open('test_map').read().split(';')
        start_y = int(start_y)
        end_y = int(end_y)
    else:
        start_y = random.randint(0, height - 1)
        end_y = random.randint(0, height - 1)
        map_str = make_map_str(width, height)

    map_ = make_map(map_str, False)

    starttime = timeit.default_timer()

    path = astar(
        map_[(0, start_y)],
        map_[(width - 1, end_y)],
        map_.get_neighbors,
        map_.get_heuristic
    )

    stop = timeit.default_timer()

    if path:
        if seed != 0:
            with open('test_map', 'w') as map_file:
                map_file.write('%s;%s;%s;\n%s' %
                               (seed, start_y, end_y, map_str))
        print('found path at seed:', seed)
        break

    seed += 1
    random.seed(seed)


print('mov_cost:', sum([tile.mov_cost for tile in path]))
print('lenght:', len(path))
print('runtime:', stop - starttime)
print_result(map_str, path)
