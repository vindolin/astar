# AStar

A* algorithm using heapq (priority queue).

*This code is work in progress!*

Run the example program with:

	python test.py # generate new map
	python test.py 0 # load last map
	python test.py 42 # use 42 as the random seed
